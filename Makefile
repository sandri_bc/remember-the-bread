.PHONY: test

clean:
	@-find . -name '*.pyc' -delete
	@-find . -name '*.pyo' -delete
	@-find . -name '*~' -delete
	@-find . -name '__pycache__' -delete
	@-rm -rf .coverage
	@-rm -rf htmlcov
	@-rm -rf coverage-reports

test:
	py.test

test-coverage:
	coverage run --source=src -m pytest
	coverage report -m --fail-under 80
	coverage xml -o coverage-reports/report.xml

lint:
	flake8 src

install:
	pip install -r requirements.txt

install-dev:
	pip install -r requirements-dev.txt
